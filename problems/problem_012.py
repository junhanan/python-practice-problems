# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def fizzbuzz(number):
#     #return fizzbuzz if divisible by 3 and 5
#     if number % 3 == 0 and number % 5 == 0:
#         return "fizzbuzz"
#     #fizz only by 3
#     elif numbeer % 3 == 0:
#         return "fizz"
#     elif number % 3 == 0:
#         return "buzz"
#     else:
#         return number
#     #buzz only by 5
#     #not divisible by either return number

def fizzbuzz(number):
    divBy3 = number % 3 == 0
    divBy5 = number % 5 == 0

    if divBy3 and divBy5:
        return "fizzbuzz"
    elif divBy3:
        return "fizz"
    elif divBy5:
        return "buzz"
    else:
        return number

print(fizzbuzz(20))
