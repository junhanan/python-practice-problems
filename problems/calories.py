def calories(entree_num, side_num, dessert_num, drink_num):
    entree = {
        1: ["Hamburger", 522],
        2: ["Veggie burger", 399],
        3: ["Impossible burger", 501]
    }
    side = {
        1: ["French Fries", 130],
        2: ["Sweet Potato Fries", 125],
        3: ["Salad", 72]
    }
    dessert = {
        1: ["Apple Pie", 222],
        2: ["Milkshake", 391],
        3: ["Fruit Cup", 100]
    }
    drink = {
        1: ["Diet Soft Drink", 10],
        2: ["Coffee", 8],
        3: ["Martini", 120]
    }
    # entree_cal = entree[entree_num][1]
    # side_cal = side[side_num][1]
    # dessert_cal = dessert[dessert_num][1]
    # drink_cal = drink[drink_num][1]
    if entree_num == 0:
        entree_cal = 0
    else:
        entree_cal = entree[entree_num][1]

    if side_num == 0:
        side_cal = 0
    else:
        side_cal = side[side_num][1]

    if dessert_num == 0:
        dessert_cal = 0
    else:
        dessert_cal = dessert[dessert_num][1]

    if drink_num == 0:
        drink_cal = 0
    else:
        drink_cal = drink[drink_num][1]

    return entree_cal + side_cal + dessert_cal + drink_cal


print(calories(2, 3, 0, 3))
