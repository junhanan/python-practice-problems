# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def minimum_value(value1, value2):
#     #return the minimum value
#     if value1 < value2:
#         return value1
#     elif value2 < value1:
#         return value2
#     # return either if the values are the same
#     else:
#         return "either"

# print(minimum_value(6,6))

class InvoiceItem:
    def __init__(self, price, quantity):
        self.price = price
        self.quantity = quantity

    def get_total(self):
        total = self.price * self.quantity
        return total


item1 = InvoiceItem(5.00, 6)
#item1 stands in for self
print(item1.quantity)
print(item1.get_total())

class Invoice:
    def __init__(self, tax_rate):
        self.tax_rate = tax_rate
        #initializing an empty list
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    def get_subtotal(self):
        #not including the self to subtotal because it is nested within this functionality
        subtotal = 0
        for item in self.items:
            subtotal += item
        return subtotal

    def get_tax(self):
        tax = self.tax_rate * self.get_subtotal()
        return tax

    def get_total(self):
        total = self.get_subtotal() + self.get_tax()
        return total

invoice1 = Invoice(0.1)
item1_total = item1.get_total()
invoice1.add_item(item1_total)
print(invoice1.get_total())
