# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == []:
        return None

    max_list = []
    for i in values:
        if i > values[0]:
            max_list.append(i)
    return max_list[0]

values = [5,43,2,5,3]
print(max_in_list(values))


#another way. I think the second way is a simpler method, but i can't
#seem to wrap my head around it yet

def max_in_list(values):
    if len(values) == 0:
        return None

    max_value = values[0]

    for item in values:
        if item > max_value:
            max_value = item
    return max_value
