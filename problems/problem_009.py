# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #reverse the letters in the word
    reversedWord = word[slice(None,None, -1)]
    ## you can also simply reverse like
    #reversed_word = reversed(word)
    #compare the two words
    if reversedWord == word:
        return "it is a palindrome"
    else:
        return "No it is not a palindrome"
    #return if it is a palindrome or not


print(is_palindrome("racecar"))
print(is_palindrome("yomama"))
