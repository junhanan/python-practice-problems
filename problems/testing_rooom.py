# def monty(swaps):
#     card = ["left", "middle", "right"]
#     for i in swaps:
#         if i == "L":
#             card[0], card[1] = card[1], card[0]
#         elif i == "R":
#             card[1], card[2] = card[2], card[1]
#         elif i == "O":
#             card[0], card[2] = card[2], card[0]
#     i_of_q = card.index("middle")
#     if i_of_q == 0:
#         return "left"
#     elif i_of_q == 1:
#         return "middle"
#     else:
#         return "right"

#     print(monty("LOL"))


    # L 1 --> 2
    # card[0], card[2] = card[2], card[0]
    # R 2 --> 3
    # card[1], card[2] = card[2], card[1]
    # O 1 --> 3
    # card[0], card[2] = card[2], card[0]


# def num_same_spaces(yesterday, today):
#     sum = 0
#     for i in yesterday:
#         for j in today:
#             if yesterday[i]== today[j]:
#                 sum += 1
#             return sum

# def new_hope(num_fars):
#     if 1 > num_fars:
#         num_fars = 1
#     elif num_fars > 6:
#         num_fars = 6
#     far_str = ("far" for i in range(num_fars))
#     far_joined = ", ".join(far_str)
#     return "A long time ago in a galaxy " + far_joined + " away..."


# def cash_on_hand(expenses):
#     # starts with $30
#     #gains $30 every month
#     #[0,1,2,3,4] gets subtracted everymonth and gets carried over
#     total_allowance = 30 * (len(expenses))
#     total_expenses = 0
#     for i in expenses:
#         total_expenses += i
#     return total_allowance - total_expenses + 30

# print(cash_on_hand([10, 20, 30, 40, 45]))


def valid_password(password):
    lower = 0
    upper = 0
    num = 0
    for i in password:
        if i.islower():
            lower += 1
        elif i.isupper():
            upper += 1
        elif i.isdigit():
            num += 1

    if len(password) >= 8 and len(password) <= 12:
        if lower > 1:
            if upper > 2:
                if num > 1:
                    return "good"
    return "bad"
# must have 8-12 characters
# at least 2 lowercase letters

# at least 3 uppercase letters
# at least two digits

def num_same_spaces(yesterday, today):
    sum = 0
    for i in range(len(yesterday)):
        if yesterday[i] == today[i] and yesterday[i] == "C":
            sum += 1
    return sum


def grade_scantron(submission, answer_key):
    score = 0
    for i in range(len(answer_key)):
        if submission[i] == answer_key[i]:
            score += 1
    return score

def language(text):
    #ei > ie "German"
    #ie > ei "English"
    #ie = ei "Maybe French?"
    num_ei = 0
    num_ie = 0
    for i in range(len(text)):
        if i == "ei":
            num_ei += 1
        elif i == "ie":
            num_ie += 0
    if num_ei > num_ie:
        return "German"
    elif num_ei < num_ie:
        return "English"
    else:
        return "Maybe French?"


# def language(text):
#     #ei > ie "German"
#     #ie > ei "English"
#     #ie = ei "Maybe French?"
#     num_ei = text.count("ei")
#     num_ie = text.count("ie")

#     if num_ei > num_ie:
#         return "German"
#     elif num_ei < num_ie:
#         return "English"
#     else:
#         return "Maybe French?"


def calculate_playlist(button_pushes):
    button = ["A", "B", "C", "D", "E"]
    for i in button_pushes:
        if i == 1:
            button[0], button[1] = button[1], button[0]
        elif i == 2:
            button.append(button[0])
            button.pop(0)
        else:
            button.insert(0, button[len(button)-1])
            button.pop(len(button)-1)
    return button
    #btn 1 swwaps the first 2 songs
    button[0], button[1] = button[1], button[0]
    #btn 2 moves the 1st song to the end
    button.append(button[0])
    button.remove()
    #btn 3 moves the last to the 1st
    button.insert(0, button[len(button)-1])
    button.pop(len(button)-1)


def half_time_scores(
    game_length,  # in minutes
    team_1_score_times,  # each value is seconds
    team_2_score_times   # each value is seconds
):
    half_time = (game_length * 60) // 2
    team1 = []
    team2 = []

    for i in team_1_score_times:
        if i < half_time:
            team1.append(i)
    for i in team_2_score_times:
        if i < half_time:
            team2.append(i)
    team1_score = len(team1)
    team2_score = len(team2)
    total_point = team1_score + team2_score
    output_str = str(total_point) + " points scored\n" \
             + "Team 1 scored " + str(len(team1)) + " points in the first " + str(half_time) + " seconds\n\n" \
             + "Team 2 scored " + str(len(team2)) + " points in the first " + str(half_time) + " seconds."
    return output_str

print(half_time_scores(48, [300, 900, 1500], [600, 1200]))

def mode(numbers):
    freq = {}
    max_count = 0
    for num in numbers:
        if num not in freq:
            freq[num] = 0
        freq[num] += 1
        max_count = max(freq.values())

    modes = []
    for key, value in freq.items():
        if value == max_count:
            modes.append(key)

print(mode([1,2,3,2,4,5,2,3,3]))

def find_missing_registrant(registrants, finishers):
    missing = []
    for i in registrants:
        if i not in finishers:
            miss = registrants.pop(i)
    return miss
