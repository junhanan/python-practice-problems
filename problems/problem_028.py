# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

str = "abcdabcbabcabcdbcabdbcabd"

def remove_duplicate_letters(s):
    lst = []
    for i in s:
        if i not in lst:
            lst.append(i)
    # this will convert the list into a string
    return "".join(lst)

print(remove_duplicate_letters(str))

#another way to do this

def remove_duplicate_letters(s):
    uniq_str = " "
    for i in s:
        if i not in uniq_str:
            uniq_str += i
    return uniq_str

print(remove_duplicate_letters(str))
